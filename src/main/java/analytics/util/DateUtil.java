package analytics.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class DateUtil {
    public static String formatDate(String date, String fromFormat, String toFormat) {
        Date date1 = null;
        try {
            date1 = new SimpleDateFormat(fromFormat).parse(date);
        }catch (Exception e){
            //TODO logging
        }
        DateFormat dateFormat = new SimpleDateFormat(toFormat);
        return dateFormat.format(date1);
    }
    public static long getDateDifference(String date1, String date2, String format){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
        try {
            long diff = simpleDateFormat.parse(date1).getTime() - simpleDateFormat.parse(date2).getTime();
            return TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return 0;
    }
    public static String getDayOftheWeekFromDate(String date, String dateFormat){
        return formatDate(date, dateFormat, "E");
    }
}
