package analytics;

import analytics.bolt.AirBolt;
import org.apache.storm.Config;
import org.apache.storm.StormSubmitter;
import org.apache.storm.generated.AlreadyAliveException;
import org.apache.storm.generated.AuthorizationException;
import org.apache.storm.generated.InvalidTopologyException;
import org.apache.storm.kafka.KafkaSpout;
import org.apache.storm.kafka.SpoutConfig;
import org.apache.storm.kafka.ZkHosts;
import org.apache.storm.topology.TopologyBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ConsumerTopology {
    private static Logger LOGGER = LoggerFactory.getLogger(ConsumerTopology.class);
    public static void main(String[] args) throws InvalidTopologyException, AuthorizationException, AlreadyAliveException {
        Properties props;
        if(args.length>0) {
            props = loadProperties(args[0]);
            for (String cmd_prop : args) {
                if (cmd_prop.equals(args[0])) continue;
                props.put(cmd_prop.split("=")[0], cmd_prop.split("=")[1]);
            }
        } else {
            LOGGER.error("Environment property not set. Specify argument dev/prod");
            return;
        }
        String topic  = (String)props.get("topic");
        String groupId = (String)props.get("group.id");
        String zkRoot = (String)props.get("zk.root");
        String topologyName = (String)props.get("topology.name");

        ZkHosts zkHosts = new ZkHosts((String)props.get("zookeeper.hosts"), (String)props.get("broker.zk.path"));
        SpoutConfig spoutConfig = new SpoutConfig(zkHosts, topic,zkRoot , groupId);
        spoutConfig.fetchSizeBytes = 2097152;
        spoutConfig.startOffsetTime = kafka.api.OffsetRequest.LatestTime();

        KafkaSpout kafkaSpout = new KafkaSpout(spoutConfig);

        TopologyBuilder builder = new TopologyBuilder();
        builder.setSpout("kafka-spout", kafkaSpout, Integer.parseInt((String)props.get("spout.parallelism")));
        builder.setBolt("reader-bolt", new AirBolt(),Integer.parseInt((String)props.get("bolt.parallelism")))
                .shuffleGrouping("kafka-spout");
        Config conf = new Config();
        conf.setDebug(false);
        conf.setNumWorkers(Integer.parseInt((String)props.get("workers")));
        conf.setMaxSpoutPending(500);
        String env = args[0];
        conf.put("environment", env);
        conf.put("consumer-id", groupId);
        conf.put("topic", topic);
        conf.put("last_deployed", System.currentTimeMillis());
        conf.put("zk-root", zkRoot);
        conf.put("code_branch", props.get("code.branch"));
        conf.put("start_time",System.currentTimeMillis());
        conf.put("properties", props);
        StormSubmitter.submitTopologyWithProgressBar(topologyName, conf, builder.createTopology());
    }

    private static Properties loadProperties(String env) {
        Properties prop = new Properties();
        try (InputStream input = ConsumerTopology.class.getClassLoader()
                .getResourceAsStream("application-" + env + ".properties")) {
            prop.load(input);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return prop;
    }
}