package analytics.model;
import java.sql.Timestamp;

public class HotelBook {
    String cookie;
    Timestamp date;
    String id;
    int adults;
    String cashbackFlag;
    Timestamp checkIn;
    Timestamp checkOut;
    int children;
    int days;
    int dx;
    int roomCount;
    String starRating;
    String taRating;

    public String getCookie() {
        return cookie;
    }

    public void setCookie(String cookie) {
        this.cookie = cookie;
    }

    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getAdults() {
        return adults;
    }

    public void setAdults(int adults) {
        this.adults = adults;
    }

    public String getCashbackFlag() {
        return cashbackFlag;
    }

    public void setCashbackFlag(String cashbackFlag) {
        this.cashbackFlag = cashbackFlag;
    }

    public Timestamp getCheckIn() {
        return checkIn;
    }

    public void setCheckIn(Timestamp checkIn) {
        this.checkIn = checkIn;
    }

    public Timestamp getCheckOut() {
        return checkOut;
    }

    public void setCheckOut(Timestamp checkOut) {
        this.checkOut = checkOut;
    }

    public int getChildren() {
        return children;
    }

    public void setChildren(int children) {
        this.children = children;
    }

    public int getDays() {
        return days;
    }

    public void setDays(int days) {
        this.days = days;
    }

    public int getDx() {
        return dx;
    }

    public void setDx(int dx) {
        this.dx = dx;
    }

    public int getRoomCount() {
        return roomCount;
    }

    public void setRoomCount(int roomCount) {
        this.roomCount = roomCount;
    }

    public String getStarRating() {
        return starRating;
    }

    public void setStarRating(String starRating) {
        this.starRating = starRating;
    }

    public String getTaRating() {
        return taRating;
    }

    public void setTaRating(String taRating) {
        this.taRating = taRating;
    }
}