package analytics.model;

public class SearchResultModel {
    private String searchId;
    private String origin;
    private String dest;
    private String sector;
    private String domain;
    private boolean intl;
    private String channel;
    private String cabinClass;
    private String departDate;
    private String returnDate;
    private String journeyType;
    private String searchTime;
    private String searchDay;
    private int pax;
    private String paxType;
    private long dx;
    private String userId;
    private String expiredSuppliers;

    public String getSearchId() {
        return searchId;
    }

    public void setSearchId(String searchId) {
        this.searchId = searchId;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getDest() {
        return dest;
    }

    public void setDest(String dest) {
        this.dest = dest;
    }

    public String getSector() {
        return sector;
    }

    public void setSector(String sector) {
        this.sector = sector;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public boolean isIntl() {
        return intl;
    }

    public void setIntl(boolean intl) {
        this.intl = intl;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getCabinClass() {
        return cabinClass;
    }

    public void setCabinClass(String cabinClass) {
        this.cabinClass = cabinClass;
    }

    public String getDepartDate() {
        return departDate;
    }

    public void setDepartDate(String departDate) {
        this.departDate = departDate;
    }

    public String getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(String returnDate) {
        this.returnDate = returnDate;
    }

    public String getJourneyType() {
        return journeyType;
    }

    public void setJourneyType(String journeyType) {
        this.journeyType = journeyType;
    }

    public String getSearchTime() {
        return searchTime;
    }

    public void setSearchTime(String searchTime) {
        this.searchTime = searchTime;
    }

    public String getSearchDay() {
        return searchDay;
    }

    public void setSearchDay(String searchDay) {
        this.searchDay = searchDay;
    }

    public int getPax() {
        return pax;
    }

    public void setPax(int pax) {
        this.pax = pax;
    }

    public String getPaxType() {
        return paxType;
    }

    public void setPaxType(String paxType) {
        this.paxType = paxType;
    }

    public long getDx() {
        return dx;
    }

    public void setDx(long dx) {
        this.dx = dx;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getExpiredSuppliers() {
        return expiredSuppliers;
    }

    public void setExpiredSuppliers(String expiredSuppliers) {
        this.expiredSuppliers = expiredSuppliers;
    }

    @Override
    public String toString() {
        return "SearchResultModel{" +
                "searchId='" + searchId + '\'' +
                ", origin='" + origin + '\'' +
                ", dest='" + dest + '\'' +
                ", sector='" + sector + '\'' +
                ", domain='" + domain + '\'' +
                ", intl=" + intl +
                ", channel='" + channel + '\'' +
                ", cabinClass='" + cabinClass + '\'' +
                ", departDate='" + departDate + '\'' +
                ", returnDate='" + returnDate + '\'' +
                ", journeyType='" + journeyType + '\'' +
                ", searchTime='" + searchTime + '\'' +
                ", searchDay='" + searchDay + '\'' +
                ", pax=" + pax +
                ", paxType='" + paxType + '\'' +
                ", dx=" + dx +
                ", userId='" + userId + '\'' +
                ", expiredSuppliers='" + expiredSuppliers + '\'' +
                '}';
    }
}
