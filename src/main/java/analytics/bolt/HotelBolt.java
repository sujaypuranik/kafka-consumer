package analytics.bolt;

import com.cleartrip.analytics.Header;
import com.cleartrip.analytics.Hotels;
import com.google.protobuf.InvalidProtocolBufferException;
import org.apache.storm.topology.BasicOutputCollector;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseBasicBolt;
import org.apache.storm.tuple.Tuple;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HotelBolt extends BaseBasicBolt {
    private static Logger LOGGER = LoggerFactory.getLogger(HotelBolt.class);

    @Override
    public void execute(Tuple input, BasicOutputCollector collector) {
        try {
            Hotels.HotelMessage hotelMessage = Hotels.HotelMessage.parseFrom(input.getBinary(0));
            if (hotelMessage.getMessageType() == Hotels.HotelMessage.HotelMessageType.HOTEL_BOOK) {
                parseMessage(hotelMessage);
            }
        } catch (InvalidProtocolBufferException e) {
            e.printStackTrace();
        }
    }

    private void parseMessage(final Hotels.HotelMessage hotelMessage) {
        Header.LogHeader header = hotelMessage.getHeader();
        if (header.getTripId().equalsIgnoreCase("200217574470")) {
            LOGGER.info("FOUND >>>>  {} ",hotelMessage.toString());
        }
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {

    }
}
