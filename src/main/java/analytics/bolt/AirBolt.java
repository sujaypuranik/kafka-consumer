package analytics.bolt;

import com.cleartrip.analytics.Air;
import com.cleartrip.analytics.Header;
import com.cleartrip.analytics.Hotels;
import com.google.protobuf.InvalidProtocolBufferException;
import org.apache.storm.topology.BasicOutputCollector;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseBasicBolt;
import org.apache.storm.tuple.Tuple;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class AirBolt extends BaseBasicBolt {
    private static Logger LOGGER = LoggerFactory.getLogger(AirBolt.class);

    @Override
    public void execute(Tuple input, BasicOutputCollector collector) {
        try {
            Air.AirMessage airMessage = Air.AirMessage.parseFrom(input.getBinary(0));
            LOGGER.info(airMessage.toString());
        } catch (InvalidProtocolBufferException e) {
            e.printStackTrace();
        }
    }

    private void parseMessage(final Air.AirMessage airMessage) {
//        LOGGER.info("message --------- {} --------", airMessage.toString());
        Header.LogHeader header = airMessage.getHeader();
        Air.AirBook airBook = airMessage.getAirBook();
        Air.AirItinerary itinerary = airBook.getItinerary();
        discountOrCashback(itinerary.getPricingsList());
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {

    }

    public static int discountOrCashback(List<Air.AirPricing> pricingList) {
        int discountCashback = 0;
        if(pricingList != null){
            for(Air.AirPricing pricing : pricingList) {
                List<Air.AirFare> faresList = pricing.getFaresList();
                for (Air.AirFare fare : faresList) {
                    LOGGER.info("Fare >> : {}", fare.toString());
                    if(fare.getCashback()>0 || fare.getDiscount()>0)
                        discountCashback = 1;
                }
            }
        }

        return discountCashback;
    }


}
